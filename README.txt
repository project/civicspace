
CIVICSPACE THEME README
v0.2 Trae McCombs 
Released with CivicSpace 0.8.2 under the AGPL license
Last updated 2005-11-10 9:20:02 AM 

RELEASE NOTES
=============

The CivicSpace theme is taking a new direction.  We ultimately hope to
have a theme that everyone can use as a good "base" theme for their
theming needs.  We have developed a system of Layouts, and Skins. 


Features
--------

* Layouts - pre-built templates for you to stack a skin on top of.
* Skins - built on top of a pre-existing layout, or create a new layout!
* no more hacks for previews!
* rich body classes for styling different node type or pages
* 1, 2, or 3 column source-ordered fluid columns 

COMMENTS
========
This theme is now broken up into these divisions:

/styles/layout.css          Defines columnar layout styles for screen viewing
/styles/utility.css         This stylesheet provides basic, generic styles 
                            that can be used in all CivicSpace themes.

/global/print/
/global/print/print.css     Print stylesheet for printer output;
                            toggle print preview with &print querystring
  

/layouts                    The base directory for all "templates"
/layouts/$LAYOUT            Where $LAYOUT is a specific layout.
/layouts/republica/         An example layout.

/layouts/$LAYOUT/$SKIN      Each layout can have multiple skins built
                            on top of it.  The Skin is how the theme looks
                            while the layout is how things are positioned.

/layouts/republica/democratica
                            The Layout(Republica) has a Skin(Democratica)
                            and it is nestled inside of that layouts directory
                            The Republica layout may have more than one skin.

custom.css                  The file your skin will utilize to create the
                            ultimate look and feel of your theme.


These types of objects are styled as groups:

#branding
#navigation
  * menus
  * primary & secondary links
  #local-tasks
    #primary-local-tasks
    #secondary-local-tasks
#content
#notices
#help
#messages
* forms
* lists
* inline elements


RECOMMENDATIONS
===============
* set your date format to something nice looking under admin/settings and
  "Date settings". Set it to something like May 20, 2005 18:30. Hopefully
  we'll get nicer date formats in the future.


CONTACT US
==========
This theme is now a group effort.  If you would like to help us make
this theme better, please join #cstheme on irc.freenode.net 

TO DO
=====
Information on features, bugs, and the future can be found here:

http://civicspacelabs.org/home/CivicSpaceThemePlan


