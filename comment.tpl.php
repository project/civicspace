<?php /* $Id$ */ ?>
<div class="comment <?php print ($comment->new) ? 'comment-new' : '' ?>">
  <div class="header">
    <h3 class="title">
      <?php print $comment->subject; ?>
      <?php if ($comment->new) : ?>
        <a id="new" name="new" class="text-replacement"><?php phptemplate_mark(); ?></a>
      <?php endif; ?>
    </h3>
    <div class="meta">
      <small>
      <?php 
        // output permalink
        if ($comment->cid) { 
          print l('#'. $comment->cid, 'node/'. $comment->nid, array('class' => 'icon icon-comment comment-permalink','title' => 'Permalink to this comment'), NULL, 'comment-'. $comment->cid);
        } 
      ?>
      <?php print "\n" .t('<span class="comment-date">On ' . str_replace('-', ' ', format_date($comment->timestamp)) . '</span> <span class="comment-author">'. format_name($comment) .'</span> said,'); ?>
      </small>
    </div>
  </div>
  <div class="content">
    <?php if ( $picture) : ?>
    <div class="user-picture">
      <?php print $picture ?>
    </div>
    <?php endif; ?>
    <?php print civicspace_word_split(phptemplate_wrap_content($content)); ?>
  </div>
  <div class="footer">
    <div class="links">
      <?php print $links; ?>
    </div>    
  </div>
</div>
