
CHANGELOG
=========
September 2, 2008 - Robin Monks
- I got premission from CivicSpace to asume ownership of this project
- Porting to Drupal 6.x
  + Created new .info file
  + Removed box.tpl.php and block.tpl.php, since those are no longer required
  
-- Old Changelog --
- Pointed Themes Help to new Help page.
	+ code by: m3avrck
- Removed all mentions of Groundswell
- Added new logo for the theme.

