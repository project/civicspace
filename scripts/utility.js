
// functions for clearing and restoring input field values

function clearText(thefield) { 
  if (thefield.defaultValue == thefield.value) 
  {
    thefield.value = ""; // if the field hasn't been edited, clear it
  }
}
function replaceText(thefield) { 
  if (thefield.value == "") 
  {
    thefield.value = thefield.defaultValue; // if the field hasn't been edited, restore the default value
  }
}
