
function toggle( targetId ) {
   if ( document.getElementById ) {
    target = document.getElementById( targetId );
    if ( target.style.display == "none" ) {
     target.style.display = "";
    } else {
     target.style.display = "none";
    }
   }
}
		
function toggleShow( targetId ) {
	if ( document.getElementById ) {
  	target = document.getElementById( targetId );
	  	if (target.style.display == "none") {
			target.style.display = "";
		} 
	}
}

function toggleHide( targetId ) {
	if ( document.getElementById ) {
  	target = document.getElementById( targetId );
	  	if (target.style.display == "") {
			target.style.display = "none";
		} 
	}
}

function toggleAll( targetTag, showHide ) {
  if ( document.getElementById ) {
    target = document.getElementsByTagName( targetTag );
    var i;
    var max = target.length;
    if ( showHide == "show" ) {
      for ( i = 0; i < max; i++ ) {
        if ( target[ i ].style.display == "none" ) {
          target[ i ].style.display = "";
        }
      }
    }
    else if ( showHide == "hide" ) {
      for ( i = 0; i < max; i++ ) {
        if ( target[ i ].style.display == "" ) {
          target[ i ].style.display = "none";
        }
      }
    }
  }      
}
