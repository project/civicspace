<?php

/*
  we need a much more robust hyphenation algorithm 
  (i.e. to cut URLs off at the slashes)!
  */
function _civicspace_word_split($text, $max_char = 30) {
  return preg_replace('/((?:[^ &]|&[^;]+;){' . $max_char . '})(?=[^ ])/u', '\1<wbr />', $text[0]);
}

function civicspace_word_split($text) {
  return substr(preg_replace_callback('/>[^<]+</', '_civicspace_word_split', '>'. $text .'<'), 1, -1);
}

/* Checks to see if we're in the admin section */
function civicspace_is_admin() {
  if ((arg(0) == 'admin') && (user_access('access administration pages'))) {
    return true;
  }
}

/* Generates path to style; similar to path_to_theme() */
function civicspace_path_to_style() {
  global $theme_key;
  $themes = list_themes();
  return dirname($themes[$theme_key]->filename);
}

/* generates a list of body classes for bad-ass per-page CSS hacking */
function civicspace_get_body_classes($sidebar_left, $sidebar_right) {
global $node;

  $body_classes = array();

  if (strlen($sidebar_left) && strlen($sidebar_right)) {
    $body_classes[] = 'three-column';
  }
  else if (strlen($sidebar_left)) {
    $body_classes[] = 'two-column';
  }
  else if (strlen($sidebar_right)) {
    $body_classes[] = 'two-column-right';
  }
  else {
    $body_classes[] = 'one-column';
  }

  if ($_GET['q'] == variable_get('site_frontpage', 'node')) {
    $body_classes[] = 'home'; 
  }

  if (arg(1)) {
    $body_classes[] = check_plain(arg(0)) . '-' . check_plain(arg(1));
  }
  if (isset($node->type)) {
    $body_classes[] = $node->type;
  }
  $path_class = explode('/', drupal_get_path_alias(check_plain($_GET['q'])));
  $body_classes[] = reset($path_class);
  $body_classes[] = preg_replace('/-\d+/', '', str_replace('/', '-', check_plain($_GET['q'])));
  $body_classes[] = 'page-' . str_replace('/', '-', check_plain($_GET['q']));
  
  return $body_classes;
}

/**
 * =Comments
 */

function phptemplate_comment_view($comment, $links = '', $visible = 1) {

  // Emit selectors:
  $output = '';
  global $comment_new;
  if (($comment->new = node_mark($comment->nid, $comment->timestamp)) != MARK_READ && !$comment_new) {
     $output .= "<a id=\"new\"></a>\n";
     $comment_new = TRUE;
  }

  if (@$comment->cid) {
    $output .= "<a id=\"comment-$comment->cid\"></a>\n";    
  }
  
  // Switch to folded/unfolded view of the comment
  if ($visible) {
    $comment->comment = check_output($comment->comment, $comment->format);
    $output .= theme('comment', $comment, $links);
  }
  else {
    $output .= theme('comment_folded', $comment);
  }

  return $output;
} 

function phptemplate_comment_form($edit, $title) {
  global $user;

  $form .= "<a id=\"comment\"></a>\n";

  // contact information:
  if ($user->uid) {
    $form .= form_item(t('Your name'), format_name($user));
  }
  else if (variable_get('comment_anonymous', 0) == 1) {
    $form .= form_textfield(t('Your name'), 'name', $edit['name'] ? $edit['name'] : variable_get('anonymous', 'Anonymous') , 20, 60);
    $form .= form_textfield(t('E-mail'), 'mail', $edit['mail'], 20, 64, t('The content of this field is kept private and will not be shown publicly.'));
    $form .= form_textfield(t('Homepage'), 'homepage', $edit['homepage'], 20, 255);
  }
  else if (variable_get('comment_anonymous', 0) == 2) {
    $form .= form_textfield(t('Your name'), 'name', $edit['name'] ? $edit['name'] : variable_get('anonymous', 'Anonymous') , 20, 60, NULL, NULL, TRUE);
    $form .= form_textfield(t('E-mail'), 'mail', $edit['mail'], 20, 64, t('The content of this field is kept private and will not be shown publicly.'), NULL, TRUE);
    $form .= form_textfield(t('Homepage'), 'homepage', $edit['homepage'], 20, 255);
  }

  // subject field:
  if (variable_get('comment_subject_field', 1)) {
    $form .= form_textfield(t('Subject'), 'subject', $edit['subject'], 50, 64);
  }

  // comment field:
  $form .= form_textarea(t('Comment'), 'comment', $edit['comment'] ? $edit['comment'] : $user->signature, 70, 10, '', NULL, TRUE);

  // format selector
  $form .= filter_form('format', $edit['format']);

  // preview button:
  $form .= form_hidden('cid', $edit['cid']);
  $form .= form_hidden('pid', $edit['pid']);
  $form .= form_hidden('nid', $edit['nid']);

  $form .= form_submit(t('Preview comment'));

  // Only show post button if preview is optional or if we are in preview mode.
  // We show the post button in preview mode even if there are form errors so that
  // optional form elements (e.g., captcha) can be updated in preview mode.
  if (!variable_get('comment_preview', 1) || ($_POST['op'] == t('Preview comment')) || ($_POST['op'] == t('Post comment'))) {
    $form .= form_submit(t('Post comment'));
  }

  return theme('box', $title, form($form, 'post', url('comment/reply/'. $edit['nid'])));
}

function phptemplate_comment_thread_min($comment, $threshold, $pid = 0) {
  if (comment_visible($comment, $threshold)) {
   /* padding instead of margin to keep IE sane */
    $output  = '<div style="padding-left:'. ($comment->depth * 20) ."px;\">\n";
    $output .= theme('comment_view', $comment, '', 0);
    $output .= "</div>\n";
  }
  return $output;
}

function phptemplate_comment_thread_max($comment, $threshold, $level = 0) {
  $output = '';
  if ($comment->depth) {
   /* padding instead of margin to keep IE sane */
    $output .= '<div style="padding-left:'. ($comment->depth * 20) ."px;\">\n";
  }

  $output .= theme('comment_view', $comment, theme('links', module_invoke_all('link', 'comment', $comment, 0)), comment_visible($comment, $threshold));

  if ($comment->depth) {
    $output .= "</div>\n";
  }
  
  return $output;
}

function _phptemplate_variables($hook, $vars) {
  $vars = array();
  if ($hook == 'page') {
    foreach (array('primary', 'secondary') as $type) {
      //Get the data to populate the textfields, if the variable is not an array .. try to parse the old-style link format.
      $value = variable_get('phptemplate_'. $type .'_links', array());
      //Get the amount of links to show, possibly expanding if there are more links defined than the count specifies.
      $count = variable_get('phptemplate_'. $type .'_link_count', 5);
      $count = ($count > sizeof($value['link'])) ? $count : sizeof($value['link']);
      if (theme_get_setting('toggle_'. $type .'_links')) {
        for ($i =0; $i < $count; $i++) {
          unset($attributes);
          if (!empty($value['text'][$i])) {
            if (!empty($value['description'][$i])) {
              $attributes['title'] = $value['description'][$i];
            }
            //Custom attribute, class='first' if first anchor...
            if ($i == 0) {
              $attributes['class'] = 'first';
            }
            $links[$type][] = l($value['text'][$i], $value['link'][$i], $attributes);
            $vars[$type . '_links'] = $links[$type];
          }
        }
      }
    }
    // Set admin theme
    if (civicspace_is_admin()) {
    	$vars['template_file'] = 'page_admin';
    }
  }
  return $vars;
}

function phptemplate_item_list($items = array(), $title = NULL) {
  $output = '<div class="item-list">' ."\n";
  if (isset($title)) {
    $output .= '<h3>'. $title .'</h3>'."\n";
  }

  if (isset($items)) {
    $output .= '<ul>'."\n";
    foreach ($items as $item) {
      static $class;
      $class += 1;
      $alt = ($class % 2 == 1) ? ' ': ' class="alt"';
      $output .= "<li$alt>$item</li>\n";
    }
    $output .= '</ul>'."\n";
  }
  $output .= '</div>'."\n";
  return $output;
}

/* Return a themed submenu, typically displayed under the tabs. */
function phptemplate_submenu($links) {
  return '<div class="submenu">'. implode(' | ', $links) .'</div>';
} 


/**
 * =Messages
 */
/* altered to always output a list of messages, even if only one message is returned */
function phptemplate_status_messages() {
  if ($data = drupal_get_messages()) {
    $output = '';
    foreach ($data as $type => $messages) {
      $output .= "<div class=\"$type\">\n";
      $theme_path = path_to_theme();
      // output an image
      if ($type == 'error') {
        $output .= '<img src="'. $theme_path .'/images/icon_error.png" alt="Error icon" class="icon-error" />' ."\n";
      }
      else if ($type == 'status') {
        $output .= '<img src="'. $theme_path .'/images/icon_status.png" alt="Status icon" class="icon-status" />' ."\n";
      }
      $output .= "  <div class=\"content\">\n";
      if (count($messages) > 1) {
        $output .= "    <ul>\n";
        foreach($messages as $message) {
          $output .= '    <li>'. $message ."</li>\n";
        }
        $output .= "    </ul>\n";
      }
      else {
        $output .= $messages[0] ."\n";
      }
      $output .= "  </div>\n";
      $output .= "</div>\n";
    }
    
    return $output;
  }
} 

/**
 * =Error messages
 */
function phptemplate_error($message) {
  $output = '';
  $output .= '<div class="error">';
  $output .= '  <img src="'. path_to_theme() .'/images/icon_error.png" alt="Error icon" class="icon icon-error" />';
  $output .= "  <div class=\"content\">\n";
  $output .= $message;
  $output .= "  </div>\n";
  $output .= "</div>\n";
  return $output;
}

/**
 * =Confirmation
 */
function phptemplate_confirm($question, $path, $description = NULL, $yes = NULL, $no = NULL, $extra = NULL, $name = 'confirm') {
  drupal_set_title($question);

  if (is_null($description)) {
    $description = t('This action cannot be undone.');
  }

  $output .= '<p>'. $description ."</p>\n";
  if (!is_null($extra)) {
    $output .= $extra;
  }
  $output .= '<div class="container-inline">';
  $output .= form_submit($yes ? $yes : t('Confirm'));
  $output .= '<span class="cancel-link">'. l($no ? $no : t('Cancel and go back'), $path) .'</span>';
  $output .= "</div>\n";

  $output .= form_hidden($name, 1);
  return form($output, 'post', NULL, array('class' => 'confirmation'));
}

/**
 * =Table formating
 */
function phptemplate_table($header, $rows, $attributes = array()) {

  $output = '<div class="table-wrapper">'. "\n" .'<table'. drupal_attributes($attributes) .">\n";

  // Format the table header:
  if (is_array($header)) {
    $ts = tablesort_init($header);
    $output .= ' <tr>';
    foreach ($header as $cell) {
      $cell = tablesort_header($cell, $header, $ts);
      $output .= _phptemplate_table_cell($cell, 1);
    }
    $output .= " </tr>\n";
  }

  // Format the table rows:
  if (is_array($rows)) {
    foreach ($rows as $number => $row) {
      $attributes = array();

      // Check if we're dealing with a simple or complex row
      if (isset($row['data'])) {
        foreach ($row as $key => $value) {
          if ($key == 'data') {
            $cells = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $cells = $row;
      }

      // Modified for simpler alternating classes
      if ($number % 2) {
        if (isset($attributes['class'])) {
          $attributes['class'] .= ' alt';
        }
        else {
          $attributes['class'] = 'alt';
        }
      }

      // Build row
      $output .= ' <tr'. drupal_attributes($attributes) .'>' ."\n";
      $i = 0;
      foreach ($cells as $cell) {
        $cell = tablesort_cell($cell, $header, $ts, $i++);
        $output .= _phptemplate_table_cell($cell, 0); // internal call
      }
      $output .= " </tr>\n";
    }
  }

  $output .= "</table>\n</div>\n\n";
  return $output;
} 

function _phptemplate_table_cell($cell, $header = 0) {
  $attributes = '';

  if (is_array($cell)) {
    $data = $cell['data'];
    foreach ($cell as $key => $value) {
      if ($key != 'data')  {
        $attributes .= " $key=\"$value\"";
      }
    }
  }
  else {
    $data = $cell;
  }

  if ($header) {
    $output = "  <th$attributes>\n$data\n  </th>\n";
  }
  else {
    $output = "  <td$attributes>\n$data\n  </td>\n";
  }

  return $output;
}

/**
 * =Pager
 */
function phptemplate_pager($tags = array(), $limit = 10, $element = 0, $attributes = array()) {
  global $pager_total;
  $output = '';

  if ($pager_total[$element] > $limit) {
    $total_row_count = '';
    if ($pager_total[$element] > 1000000) {
      $total_row_count = 'millions'; 
    }
    else if ($pager_total[$element] > 1000) {
      $total_row_count = 'thousands'; 
    }
    else {
      $total_row_count = $pager_total[$element];
    }
    $output .= '<div id="pager">' ."\n";
    $output .= theme('pager_first', ($tags[0] ? $tags[0] : t('&laquo; First')), $limit, $element, $attributes); 
    $output .= theme('pager_previous', ($tags[1] ? $tags[1] : t('&#8249; Previous')), $limit, $element, 1, $attributes);
    $output .= phptemplate_pager_detail($limit, $element, 'Showing <span class="pager-record-count">%d - %d</span> of <span class="pager-total">'. $total_row_count .'</span>.');
    $output .= theme('pager_next', ($tags[3] ? $tags[3] : t('Next &#8250;')), $limit, $element, 1, $attributes);
    $output .= theme('pager_last', ($tags[4] ? $tags[4] : t('Last &raquo;')), $limit, $element, $attributes);
    $output .= "\n</div>";

    return $output;
  }
}

function phptemplate_pager_next($text, $limit, $element = 0, $interval = 1, $attributes = array()) {
  global $pager_from_array, $pager_total;
  $from_new = pager_load_array(((int)$pager_from_array[$element] + ((int)$limit * (int)$interval)), $element, $pager_from_array);
  if ($from_new[$element] < $pager_total[$element]) {
    $output .= '  <a href="'. pager_link($from_new, $element, $attributes) .'">'. $text .'</a>'."\n";
  }
  else {
    $output .= ' ';
  }
  return $output;
}

function phptemplate_pager_previous($text, $limit, $element = 0, $interval = 1, $attributes = array()) {
  global $pager_from_array;
  $from_new = pager_load_array(((int)$pager_from_array[$element] - ((int)$limit * (int)$interval)), $element, $pager_from_array);
  if ($from_new[$element] < 1) {
    $output .= theme('pager_first', $text, $limit, $element, $attributes);
  }
  else {
    $output .= '<a href="'. pager_link($from_new, $element, $attributes) .'">'. $text .'</a>';
  }
  return $output;
} 

function phptemplate_pager_first($text, $limit, $element = 0, $attributes = array()) {
  global $pager_from_array;

  if ((($pager_from_array[$element] > 0) && ($pager_from_array[$element] <= $limit*2))) {
    $output .= '<a href="'. pager_link(pager_load_array(0, $element, $pager_from_array), $element, $attributes) .'">'. $text .'</a>';
  }
  else {
    $output .= ' ';
  }
  return $output;
} 

function phptemplate_pager_last($text, $limit, $element = 0, $attributes = array()) {
  global $pager_from_array, $pager_total;

  // calculate how many items are on the last page
  $last_num = (($pager_total[$element] % $limit) ? ($pager_total[$element] % $limit) : $limit);
  // fetch pager array of pages starting from the beginning of the last page
  $from_new = pager_load_array(($pager_total[$element] - $last_num), $element, $pager_from_array);

  if ($from_new[$element] < ($pager_from_array[$element] + $limit)) {
    $output .= theme('pager_next', $text, $limit, $element, 1, $attributes);
  }
  else if (($from_new[$element] > $pager_from_array[$element]) && ($from_new[$element] > 0) && ($from_new[$element] < $pager_total[$element])) {
    $output .= '  <a href="'. pager_link($from_new, $element, $attributes) .'">'. $text .'</a>';
  }
  else {
    $output .= ' ';
  }
  return $output;
}

function phptemplate_pager_detail($limit, $element = 0, $format = '%d - %d of %d.') {
  global $pager_from_array, $pager_total;

  $output = '<div class="pager-detail">'. "\n";
  if ($pager_total[$element] > (int)$pager_from_array[$element] + 1) {
    $output .= sprintf(t($format), (int)$pager_from_array[$element] + 1, ((int)$pager_from_array[$element] + $limit <= $pager_total[$element] ? (int)$pager_from_array[$element] + $limit : $pager_total[$element]), $pager_total[$element]);  
  }
  $output .= '</div>';

  return $output;
} 

/**
 * =Forums
 */
function phptemplate_forum_list($forums, $parents, $tid) {
  global $user;

  if ($forums) {

    $header = array(t('Forum'), t('Topics'), t('Posts'), t('Last post'));

    foreach ($forums as $forum) {
      if ($forum->container) {
        $description  = '<div style="padding-left: '. ($forum->depth * 20) ."px;\">\n";
        $description .= ' <div class="name">'. "\n". l($forum->name, "forum/$forum->tid") ."\n</div>\n";

        if ($forum->description) {
          $description .= " <div class=\"description\">\n  $forum->description\n</div>\n";
        }
        $description .= "</div>\n";

        $rows[] = array(array('data' => $description, 'class' => 'container', 'colspan' => '4'));
      }
      else {
        $forum->old_topics = _forum_topics_read($forum->tid, $user->uid);
        if ($user->uid) {
          $new_topics = $forum->num_topics - $forum->old_topics;
        }
        else {
          $new_topics = 0;
        }

        $description  = '<div style="padding-left: '. ($forum->depth * 20) ."px;\">\n";
        $description .= ' <div class="name">'. l($forum->name, "forum/$forum->tid") ."</div>\n";

        if ($forum->description) {
          $description .= " <div class=\"description\">$forum->description</div>\n";
        }
        $description .= "</div>\n";

        // I would like to add a link to the most recent post in the fourth column, but this is not possible
        // See: http://drupal.org/node/23251

        $rows[] = array(
          array('data' => $description, 'class' => 'forum'),
          array('data' => $forum->num_topics . ($new_topics ? '<br />'. l(t('%a new', array('%a' => $new_topics)), "forum/$forum->tid", NULL, NULL, 'new') : ''), 'class' => 'topics'),
          array('data' => $forum->num_posts, 'class' => 'posts'),
          array('data' => _phptemplate_forum_format($forum->last_post), 'class' => 'last-reply'));
      }
    }

    return theme('table', $header, $rows);

  }

} 

function phptemplate_forum_display($forums, $topics, $parents, $tid, $sortby, $forum_per_page) {
  global $user;
  // forum list, topics list, topic browser and 'add new topic' link

  $vocabulary = taxonomy_get_vocabulary(variable_get('forum_nav_vocabulary', ''));
  $title = $vocabulary->name;

  // Breadcrumb navigation:
  $breadcrumb = array();
  if ($tid) {
    $breadcrumb[] = array('path' => 'forum', 'title' => $title);
  }

  if ($parents) {
    $parents = array_reverse($parents);
    foreach ($parents as $p) {
      if ($p->tid == $tid) {
        $title = $p->name;
      }
      else {
        $breadcrumb[] = array('path' => 'forum/'. $p->tid, 'title' => $p->name);
      }
    }
  }

  drupal_set_title($title);

  $breadcrumb[] = array('path' => $_GET['q']);
  menu_set_location($breadcrumb);
  
  /*
    This should use menu_local_tasks for these options
  */

  if (count($forums) || count($parents)) {
    $output  = '<div id="forum">';
    $output .= '<ul>';

    if (module_exist('tracker')) {
      if ($user->uid) {
        $output .= ' <li>'. l(t('My forum discussions.'), "tracker/$user->uid") .'</li>';
      }

      $output .= ' <li>'. l(t('Active forum discussions.'), 'tracker') .'</li>';
    }

    if (user_access('create forum topics')) {
      $output .= '<li>'. l(t('Post new forum topic.'), "node/add/forum/$tid") .'</li>';
    }
    else if ($user->uid) {
      $output .= '<li>'. t('You are not allowed to post a new forum topic.') .'</li>';
    }
    else {
      $output .= '<li>'. t('<a href="%login">Login</a> to post a new forum topic.', array('%login' => url('user/login'))) .'</li>';
    }
    $output .= '</ul>';

    $output .= theme('forum_list', $forums, $parents, $tid);

    if ($tid && !in_array($tid, variable_get('forum_containers', array()))) {
      drupal_set_html_head('<link rel="alternate" type="application/rss+xml" title="RSS - '. $title .'" href="'. url('taxonomy/term/'. $tid .'/0/feed') .'" />');

      $output .= theme('forum_topic_list', $tid, $topics, $sortby, $forum_per_page);
      $output .= theme('xml_icon', url("taxonomy/term/$tid/0/feed"));
    }
    $output .= '</div>';
  }
  else {
    drupal_set_title(t('No forums defined'));
    $output = '';
  }

  return $output;
}

function phptemplate_forum_topic_list($tid, $topics, $sortby, $forum_per_page) {
  global $forum_topic_list_header;

  if ($topics) {

    foreach ($topics as $topic) {
      // folder is new if topic is new or there are new comments since last visit
      if ($topic->tid != $tid) {
        $rows[] = array(
          array('data' => _forum_icon($topic->new, $topic->num_comments, $topic->comment_mode, $topic->sticky), 'class' => 'icon'),
          array('data' => check_plain($topic->title), 'class' => 'title'),
          array('data' => l(t('This topic has been moved'), "forum/$topic->tid"), 'colspan' => '3')
        );
      }
      else {
        $rows[] = array(
          array('data' => _forum_icon($topic->new, $topic->num_comments, $topic->comment_mode, $topic->sticky), 'class' => 'icon'),
          array('data' => l($topic->title, "node/$topic->nid"), 'class' => 'topic'),
          array('data' => $topic->num_comments . ($topic->new_replies ? '<br />'. l(t('%a new', array('%a' => $topic->new_replies)), "node/$topic->nid", NULL, NULL, 'new') : ''), 'class' => 'replies'),
          array('data' => _phptemplate_forum_format($topic), 'class' => 'created'),
          array('data' => _phptemplate_forum_format($topic->last_reply), 'class' => 'last-reply')
        );
      }
    }

    if ($pager = theme('pager', NULL, $forum_per_page, 0, tablesort_pager())) {
      $rows[] = array(array('data' => $pager, 'colspan' => '5', 'class' => 'pager'));
    }
  }

  $output .= theme('table', $forum_topic_list_header, $rows);

  return $output;
} 

function _phptemplate_forum_format($topic) {
  if ($topic && $topic->timestamp) {
    return t('<div="topic-posted">'. "\n" .'%time ago'. "\n" .'</div>'. "\n" .'<div="topic-author">'. "\n" .'%author</div>', array('%time' => format_interval(time() - $topic->timestamp), '%author' => format_name($topic)));
  }
  else {
    return message_na();
  }
} 


/**
 * =Mark
 */
function phptemplate_mark($type = MARK_NEW) {
  global $user;
  if ($user->uid) {
    if ($type == MARK_NEW) {
      return '<span class="marker marker-new">'. t('new') .'</span>';
    }
    else if ($type == MARK_UPDATED) {
      return '<span class="marker marker-updated">'. t('updated') .'</span>';
    }
  }
} 

/**
 * =Filter tips
 */
/* outputs collapsible filter tips */
function phptemplate_filter_tips($tips, $long = false, $extra = '') {
  static $format_id;
  $format_id += 1;
  $tips_to_hide ='';
  $output = '';

  $multiple = count($tips) > 1;
  if ($multiple) {
    $output = t('Input formats') .':';
  }

  if (count($tips)) {
    if ($multiple) {
      $output .= '<ul class="filter-list">' . "\n";
    }
    foreach ($tips as $name => $tiplist) {
      if ($multiple) {
        $output .= '<li><div class="filter-name"><strong>'. $name .'</strong>:</div>' . "\n";
      }

      $tips = '';
      foreach ($tiplist as $tip) {
        $tips .= '<li'. ($long ? ' id="filter-'. str_replace("/", "-", $tip['id']) .'">' : '>') . $tip['tip'] . '</li>'. "\n";
      }

      if ($tips) {
        if (!$multiple) {
          $output .= '<span class="toggle-button" id="show-input-formats-'. $format_id .'">'. "\n" .'  <a href="javascript:void(0);" onclick="toggleShow(\'input-formats-container-' . $format_id . '\');toggleShow(\'hide-input-formats-'. $format_id .'\');toggleHide(\'show-input-formats-'. $format_id .'\');return true;"><img src="' . path_to_theme() . '/global/images/icon_collapsed.png" alt="Expand filter description" /></a>'. "\n" .'</span>' . "\n";
          $output .= '<span class="toggle-button" style="display: none;" id="hide-input-formats-'. $format_id .'">'. "\n" .'  <a href="javascript:void(0);" onclick="toggleHide(\'input-formats-container-' . $format_id . '\');toggleHide(\'hide-input-formats-'. $format_id .'\');toggleShow(\'show-input-formats-'. $format_id .'\');return true;"><img src="' . path_to_theme() . '/global/images/icon_expanded.png" alt="Hide filter description" /></a>'. "\n" .'</span>' . "\n";
          $output .= '<div id="input-formats-container-'. $format_id .'">'. "\n";
        }

        $output .= "<ul class=\"tips\">\n$tips\n</ul>";

        if (!$multiple) {
          $output .= '</div>';
          $output .= '<script language="JavaScript1.2" type="text/javascript">'. "\n" .' window.onload = toggleHide(\'input-formats-container-' . $format_id . "');\n</script>\n";
        }
      }

      if ($multiple) {
        $output .= '</li>' . "\n";
      }
    }
    if ($multiple) {
      $output .= '</ul>'. "\n";
    }
  }
  return $output;
}

/* changed sort images */
function phptemplate_image($path, $alt = '', $title = '', $attributes = NULL, $getsize = TRUE) {
  switch ($path) {
  case 'misc/arrow-asc.png':
    $path = path_to_theme() . '/global/images/asc.png';
    break;
  case 'misc/arrow-desc.png':
    $path = path_to_theme() . '/global/images/desc.png';
    break;
  }
  if (!$getsize || (is_file($path) && (list($width, $height, $type) = @getimagesize($path)))) {
    return '<img src="'. check_url($path) .'" alt="'. check_plain($alt) .'" title="'. check_plain($title) .'" '. $attributes .' />';
  }
} 


/**
 * =Books
 */
function phptemplate_book_navigation($node) {
  $path = book_location($node);

  // Construct the breadcrumb:

  $node->breadcrumb = array(); // Overwrite the trail with a book trail.
  foreach ($path as $level) {
    $node->breadcrumb[] = array('path' => 'node/'. $level->nid, 'title' =>  $level->title);
  }
  $node->breadcrumb[] = array('path' => 'node/'. $node->nid);

  if ($node->nid) {
    $output .= '<div class="book">';

    if ($tree = book_tree($node->nid)) {
      $output .= '<div class="tree">'. $tree .'</div>';
    }

    if ($prev = book_prev($node)) {
      $links .= '<div class="prev">';
      $links .= l(t('previous'), 'node/'. $prev->nid, array('title' => t('View the previous page.')));
      $links .= '</div>';
      $titles .= '<div class="prev">'. check_plain($prev->title) .'</div>';
    }
    else {
      $links .= '<div class="prev">&nbsp;</div>'; // Make an empty div to fill the space.
    }

    if ($next = book_next($node)) {
      $links .= '<div class="next">';
      $links .= l(t('next'), 'node/'. $next->nid, array('title' => t('View the next page.')));
      $links .= '</div>';
      $titles .= '<div class="next">'. check_plain($next->title) .'</div>';
    }
    else {
      $links .= '<div class="next">&nbsp;</div>'; // Make an empty div to fill the space.
    }

    if ($node->parent) {
      $links .= '<div class="up">';
      $links .= l(t('up'), 'node/'. $node->parent, array('title' => t('View this page\'s parent section.')));
      $links .= '</div>';
    }

    $output .= '<div class="nav">';
    $output .= ' <div class="links">'. $links .'</div>';
    $output .= ' <div class="titles">'. $titles .'</div>';
    $output .= '</div>';
    $output .= '</div>';
  }

  $node->body = $node->body.$output;

  return $node;
} 

?>
