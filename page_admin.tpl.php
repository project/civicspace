<?php /* $Id$ */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	   "http://http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title>
  <?php
    if ($head_title == ""):
      print $site.' - '.t($site_slogan);
    else:
      print $head_title;
    endif;
  ?>
  </title>
  <?php if ($mission != ""): ?>
    <meta name="description" content="<?php print check_plain(strip_tags($mission)) ?>" />
  <?php endif; ?>
  <meta name="revisit-after" content="1 days" />
  <meta name="robots" content="no-follow" />

<?php 
  print $head;

  // simple way of offering print previews: just append &print to your URL
  if( isset( $_GET['print'] ) ) { 
    print phptemplate_stylesheet_import($directory .'/global/print/print.css','print, screen');
  }
  else {
    print '<link href="'. $directory .'/global/styles/utility.css" rel="stylesheet" media="all" />'. "\n";    
    // implemenation of universal admin theme

    // pseude code:
    // if file_exists(current_style/admin/style.css) {
    //   link to custom admin styles;
    // }
    // else {
    //   use the default admin styles;
    // }

    $file = civicspace_path_to_style() . '/admin/admin.css';
    if (file_exists($file)) {
      print phptemplate_stylesheet_import($file,'screen');    
    }
    else {
      print phptemplate_stylesheet_import($directory .'/admin/admin.css','screen');    
    }
  }

?>

  <!--[if IE]>
  <style type="text/css" media="screen">
    /* Give IE some :hover lovin' '*/
    body {
      behavior: url("<?php print $directory ?>/scripts/csshover.htc");
    }
    /* IE min-width trick */
    div#wrapper { 
      width:expression(((document.compatMode && document.compatMode=='CSS1Compat') ? document.documentElement.clientWidth : document.body.clientWidth) < 720 ? "720px" : "auto"); 
    }
    * {
      zoom: 1; /* IE peekaboo fix */
    }
    .outer, .main-content {
      word-wrap: break-word;
    }
  </style>
  <![endif]-->
  
  <script src="<?php print $directory ?>/scripts/pngfix.js" type="text/javascript"></script>
  <script src="<?php print $directory ?>/scripts/utility.js" type="text/javascript"></script>
  <script src="<?php print $directory ?>/scripts/toggle.js" type="text/javascript"></script>

</head>
<?php
  $body_classes = civicspace_get_body_classes($sidebar_left, $sidebar_right);
?>
<body class="<?php print implode(' ', array_unique($body_classes));?>"<?php print theme("onload_attribute"); ?>>

<div class="hide skiplink">
  <?php print l(t('Skip to the main content'), $_GET['q'],array('title' =>'Skip directly to the main content'), NULL, 'body_content', FALSE) ."\n" ?>
</div>


  <div id="admin-header">
    <h1 id="site-name">
      <?php print $site?>
      <span>
        <a href="<?php print url('') ?>">View site &raquo;</a>
      </span>
    </h1>
    <div id="admin-help">
      <ul>
        <li><a href="http://drupal.org/handbook" title="Drupal handbook">Drupal Handbook</a></li>

<?php
        $help_loc = check_plain(arg(1));
        $help_link = "?q=admin/help";
        if ($help_loc) {
          if ($help_loc == "settings" && check_plain(arg(2))) {
            $help_loc = check_plain(arg(2));
            $help_print = ucfirst($help_loc) .' Help';
            $help_loc = '/'. $help_loc;
          }
          elseif ($help_loc == "help") {
            $help_loc = "";
            $help_print = "Help Index";
          }
         elseif ($help_loc == "themes") {
           $help_loc = "";
           $help_link = "http://civicspacelabs.org/home/CivicSpaceTheme";
           $help_print = "CivicSpace Theme Help";
         }
          else {
          $help_print = ucfirst($help_loc) .' Help';
          $help_loc = '/'. $help_loc;
          }
        } else {
          $help_print = "Admin Help";
        }
        ?>
        <li><strong><a href="<?php print $help_link . $help_loc; ?>"><?php print
$help_print; ?></a></strong></li>


        <li class="last"><a href="?q=logout">Logoff</a></li>
      </ul>
    </div>
    <div class="clearing"></div>
  </div>
  
  <!-- END: HEADER
       |||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->
         

<!-- BEGIN: Content top -->
  <hr class="hide" />
  <div id="wrapper">  
    <div class="outer" id="content-holder">
      <div class="float-wrap">
        <div class="center" id="div_column_center">   
          <div id="content">
            <div class="main-content" id="main">
              <a name="body_content" id="body_content"></a>
              <!-- END: Content top -->

              <div class="breadcrumb-container no-print">
                You are here: <?php print $breadcrumb ?> 
              </div>
              
              <?php if ($title != ""): ?>
              <h1 class="page-title <?php if ($tabs != "") { print 'page-title-withtabs'; } ?>"><?php print $title ?></h1> 
              <?php endif; ?>
            
              <?php if ($mission != ""): ?>
                <div id="mission">
                  <img src="<?php print $directory ?>/images/bg_mission.png" class="alignright no-print" alt="Flag decoration" />
                  <div class="content">
                    <?php print $mission ?>
                  </div>
                </div>
              <?php endif; ?>

              <?php if (($messages != "") or ($help != "")): ?>
                <div class="messages">

                  <?php if ($help != ""): ?>
                    <div class="help-toggle no-print" id="toggle-help-show" style="display: none;">
                      <a href="javascript:void(0);" onclick="toggleShow('help');toggleShow('toggle-help-hide');toggleHide('toggle-help-show');" class="toggle-link"><img src="<?php print $directory; ?>/global/images/icon_collapsed.png" alt="Collapse" /> Show help</a>
                    </div>
                    <div class="help-toggle" id="toggle-help-hide" style="display: none;">
                      <a href="javascript:void(0);" onclick="toggleHide('help');toggleHide('toggle-help-hide');toggleShow('toggle-help-show');" class="toggle-link"><img src="<?php print $directory; ?>/global/images/icon_expanded.png" alt="Expand" /> Hide help</a>
                    </div>
                    <div id="help">
                      <img src="<?php print $directory ?>/global/images/icon_help.png" alt="Help icon" class="icon-help" />
                      <div class="content">
                        <?php print $help ?>                      
                      </div>
                    </div>
	<script type="text/javascript"><!--
		toggleHide('help'); toggleShow('toggle-help-show');
	//--></script>
                  <?php endif; ?>

                  <?php if ($messages != ""): ?>
                    <?php print $messages ?>
                  <?php endif; ?>

                </div>
              <?php endif; ?>
    
              <?php if ($tabs != "" && !(arg(0) == 'node' && is_numeric(arg(1)) && is_null(arg(2)))): ?>
                <div id="local-tasks" class="no-print">
                  <?php print $tabs ?>
                </div>
              <?php endif; ?>

              <?php print($content); ?>

          <!-- BEGIN: Content middle -->
        </div>
      </div> <!-- end content div -->    
    </div> <!-- end centered div -->
<!-- END: Content middle -->


<?php if ($sidebar_left != '') { ?>
<!-- begin left sidebar content -->
    <div class="left sidebar" id="div_column_left">
      <div class="container-left">

        <?php
          print(civicspace_word_split($sidebar_left));
          print(civicspace_word_split($sidebar_right));
        ?>

    </div> <!-- end container-left div -->
  </div> <!-- end left div -->
  <div class="clearing"></div> 
<?php } ?>
  
  </div> <!-- end float-wrap div -->
<!-- end left sidebar content -->

<!-- BEGIN: Content bottom -->
    <div class="clearing"></div> 
  </div>  <!-- end outer div -->
</div>
<!-- END: Content bottom -->


<div class="clearing"></div>

<?php print $closure ?>

</body>

</html>
