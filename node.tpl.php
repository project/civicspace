<?php /* $Id$ */ ?>
<div class="node <?php print 'node-'.$node->type; print ($sticky) ? " sticky" : ""; ?>">
  <!-- Node header -->
  <div class="header">
    <?php if (!$page) { ?>
      <h2 class="title">
        <?php if (!(arg(0) == 'node' && is_numeric(arg(1)) && is_null(arg(2)))) { ?>
          <a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a>
        <?php } else { ?>
          <?php print $title ?> <a href="<?php print $node_url ?>" rel="bookmark" class="permalink" title="Permanent link to <?php print $title ?>">#</a>
        <?php } ?>
      </h2> 
    <?php } ?>
    <?php if ($page) : ?>        
      <div class="print-preview-toggle no-print rfloat">
        <a href="<?php print url($_GET['q'], NULL, NULL, TRUE); ?>&print" class="icon-print" title="<?php print t('Format for printing') ?>"><img src="<?php print path_to_theme() ?>/global/images/icon_print.png" alt="<?php print t('Format for printing') ?>" /></a>
      </div>
    <?php endif; ?>  
    <?php if ($submitted): ?>
      <div class="meta">
        <small><?php print $submitted ?></small>
      </div>
    <?php endif; ?>  
  </div> 

  <!-- Node content -->
  <div class="content">
    <?php if ($page) : ?>
    <div id="local-tasks">
      <?php print theme('menu_local_tasks'); ?>
    </div>
    <?php endif; ?>
    
    <?php if ( $picture) : ?>
    <div class="user-picture">
      <?php print $picture ?>
    </div>
    <?php endif; ?>

    <?php print civicspace_word_split(phptemplate_wrap_content($content)); ?>
    
    <?php if (!$page && $node->readmore) { ?>
    <div class="read-more">
      <?php print l(t('Read the rest of this entry...'),'node/'. $node->nid); ?>
    </div> 
    <?php } ?>
  </div> 

  <?php if ($links): ?>
  <!-- Node footer -->
  <div class="footer">
    <div class="links">
      <p><?php if ($terms): ?> <span class="postmetadata">Posted in <?php print $terms ?></span> | <?php endif; ?><?php print $links ?> &#187;</p> 
    </div>
  </div>
  <?php endif; ?> 
</div>
