<?php /* $Id$ */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	   "http://http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title>
  <?php
    if ($head_title == ""):
      print $site.' - '.t($site_slogan);
    else:
      print $head_title;
    endif;
  ?>
  </title>
  <?php if ($mission != ""): ?>
    <meta name="description" content="<?php print check_plain(strip_tags($mission)) ?>" />
  <?php endif; ?>
  <meta name="revisit-after" content="1 days" />
  <meta name="robots" content="follow" />

<?php 
  print $head;

  // simple way of offering print previews: just append &print to your URL
  if( isset( $_GET['print'] ) ) { 
    print phptemplate_stylesheet_import($base_path.$directory .'/global/print/print.css','print, screen');
  }
  else {
    print '<link href="'. $base_path.$directory .'/global/styles/utility.css" rel="stylesheet" media="all" />'. "\n";
    print $styles ."\n";
  }

?>

  <!--[if IE]>
  <style type="text/css" media="screen">
    /* Give IE some :hover lovin' '*/
    body {
      behavior: url("<?php print $base_path.$directory ?>/scripts/csshover.htc");
    }
    /* IE min-width trick */
    div#wrapper { 
      width:expression(((document.compatMode && document.compatMode=='CSS1Compat') ? document.documentElement.clientWidth : document.body.clientWidth) < 720 ? "720px" : "auto"); 
    }
    * {
      zoom: 1; /* IE peekaboo fix */
    }
    .outer, .main-content {
      word-wrap: break-word;
    }
  </style>
  <![endif]-->
  
  <?php echo $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
  
</head>
<?php
  $body_classes = civicspace_get_body_classes($left, $right);
?>
<body class="<?php print implode(' ', array_unique($body_classes));?>"<?php print theme("onload_attribute"); ?>>
<div id="page"> <!-- start #page -->
<div class="hide skiplink">
  <?php print l(t('Skip to the main content'), $_GET['q'],array('title' =>'Skip directly to the main content'), NULL, 'body_content', FALSE) ."\n" ?>
</div>
  <!-- START: HEADER 
       |||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->
<div id="header"> <!-- start: header div -->
  <div id="branding">
    <div id="branding-container">
        <?php if (!empty($logo)): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>
    </div>
  </div>
  <?php if ($search_box) { ?>

  <div class="search-wrapper" id="header-search">
    <form action="<?php print $search_url ?>" method="post" id="search">
      <input class="form-text form-search" name="edit[keys]" id="keys" />
      <input class="form-submit" type="submit" id="submit" name="op" value="<?php print $search_button_text ?>" />
    </form>
  </div>

  <?php } /* end if */ ?>

</div> <!-- end header div -->
  <!-- END: HEADER
       |||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->
         

  <?php if ($primary_links or $secondary_links) { ?>

  <!-- START: NAV      |||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->
  <div id="navigation">
    <h3 class="hide">Site Navigation</h3>
    <?php if (is_array($primary_links) && (count($primary_links) > 0)): ?>
      <ul id="global-primary" class="global-navigation">
      <?php foreach ($primary_links as $link): ?>
        <?php
          static $primary_link_id;
          $primary_link_id += 1;    
        ?>
        <li id="primary-link-<?php print $primary_link_id; ?>"><?php print $link; ?></li>
      <?php endforeach; ?>
      </ul>
    <?php endif; ?>

    <?php if (is_array($secondary_links) && (count($secondary_links) > 0)): ?>
      <ul id="global-secondary" class="global-navigation">
      <?php foreach (array_reverse($secondary_links) as $link): ?>
        <?php
          static $secondary_link_id;
          $secondary_link_id += 1;    
        ?>
        <li id="secondary-link-<?php print $secondary_link_id; ?>"><?php print $link; ?></li>
      <?php endforeach; ?>
      </ul>
    <?php endif; ?>
  </div>
  <!-- END: NAV     |||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->  

  <?php 
    } // close primary/secondary links 
  ?>


<!-- BEGIN: Content top -->
  <hr class="hide" />
  <div id="wrapper">  
    <div class="outer" id="content-holder">
      <div class="float-wrap">
        <div class="center" id="div_column_center">   
          <div id="content">
            <div class="main-content" id="main">
              <a name="body_content" id="body_content"></a>
              <!-- END: Content top -->

              <?php if ($title != ""): ?>
              <h1 class="page-title <?php if ($tabs != "") { print 'page-title-withtabs'; } ?>"><?php print $title ?></h1> 
              <?php endif; ?>
            
              <?php if ($mission != ""): ?>
                <div id="mission">
                  <img src="<?php print $base_path.$directory ?>/images/bg_mission.png" class="alignright no-print" alt="Flag decoration" />
                  <div class="content">
                    <?php print $mission ?>
                  </div>
                </div>
              <?php endif; ?>

              <?php if (($messages != "") or ($help != "")): ?>
                <div class="messages">

                  <?php if ($help != ""): ?>
                    <div class="help-toggle no-print" id="toggle-help-show" style="display: none;">
                      <a href="javascript:void(0);" onclick="toggleShow('help');toggleShow('toggle-help-hide');toggleHide('toggle-help-show');" class="toggle-link"><img src="<?php print $base_path.$directory; ?>/global/images/icon_collapsed.png" alt="Collapse" /> Show help</a>
                    </div>
                    <div class="help-toggle" id="toggle-help-hide" style="display: none;">
                      <a href="javascript:void(0);" onclick="toggleHide('help');toggleHide('toggle-help-hide');toggleShow('toggle-help-show');" class="toggle-link"><img src="<?php print $base_path.$directory; ?>/global/images/icon_expanded.png" alt="Expand" /> Hide help</a>
                    </div>
                    <div id="help">
                      <img src="<?php print $base_path.$directory ?>/global/images/icon_help.png" alt="Help icon" class="icon-help" />
                      <div class="content">
                        <?php print $help ?>                      
                      </div>
                    </div>
        <script type="text/javascript"><!--
                toggleHide('help'); toggleShow('toggle-help-show');
        //--></script>
                  <?php endif; ?>

                  <?php if ($messages != ""): ?>
                    <?php print $messages ?>
                  <?php endif; ?>

                </div>
              <?php endif; ?>
    
              <?php if ($tabs != "" && !(arg(0) == 'node' && is_numeric(arg(1)) && is_null(arg(2)))): ?>
                <div id="local-tasks" class="no-print">
                  <?php print $tabs ?>
                </div>
              <?php endif; ?>

              <?php print($content); ?>

          <!-- BEGIN: Content middle -->
        </div>
      </div> <!-- end content div -->    
    </div> <!-- end centered div -->
<!-- END: Content middle -->


<?php if ($left != '') { ?>
<!-- begin left sidebar content -->
    <div class="left sidebar" id="div_column_left">
      <div class="container-left">

        <?php print(civicspace_word_split($left)); ?>

    </div> <!-- end container-left div -->
  </div> <!-- end left div -->
  <div class="clearing"></div> 
<?php } ?>
  
  </div> <!-- end float-wrap div -->
<!-- end left sidebar content -->

<?php if (($right != '')) { ?>
<!-- begin right sidebar content -->
  <div class="right sidebar" id="div_column_right">
    <div class="container-right">

      <?php print(civicspace_word_split($right)); ?>

    </div> <!-- end container-right div -->
    <div class="clear"></div>
  </div> <!-- end right div -->
<!-- end right sidebar content -->
<?php } ?>

<!-- BEGIN: Content bottom -->
    <div class="clearing"></div> 
  </div>  <!-- end outer div -->
</div>
<!-- END: Content bottom -->


<div class="clearing"></div>

<!-- START: FOOTER     |||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->
<?php if (($_GET['q']) != variable_get('site_frontpage','node') && $breadcrumb): /* this prevents breadcrumb from showing up on homepage */ ?>
  <div class="breadcrumb-container no-print">
    You are here: <?php print $breadcrumb ?>
  </div>
<?php endif; ?>

<div id="notices">
  <?php if ($footer_message): ?>
  <div id="footer">
    <?php print $footer_message; ?>
  </div>
  <?php endif; ?>

  <hr class="hide" />

  <?php if ($primary_links or $secondary_links): ?>
    <?php if (is_array($primary_links)): ?>
      <ul class="global-navigation no-print">
      <?php foreach ($primary_links as $link): ?>
        <li><?php print $link; ?></li>
      <?php endforeach; ?>        
      </ul>
    <?php endif; ?>
  <?php endif; ?>
  <?php if( isset( $_GET['print'] ) ) : ?>
  <div class="print-preview-toggle print-only">
    <a href="<?php print url($_GET['q'], NULL, NULL, TRUE); ?>" style="@print display: none; float: right;">Return to web view</a>
    <small>
    <!--Insert dated printed and URL--> This page was viewed on <?php print format_date(time(), "custom", "Y-m-d H:i O"); ?> and can be found at <?php print url($_GET['q'], NULL, NULL, TRUE); ?>.
    </small>
  </div>
  <?php endif; ?>
</div>
<!-- END: FOOTER      |||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->

<?php print $closure ?>

<?php
  // focus for certain pages
 if (arg(0) == 'user') {
    print '<script>if ( document.getElementById ) {window.onload = document.getElementById(\'edit-name\').focus();}</script>';
  }
?>
</div> <!-- end #page -->
</body>

</html>
